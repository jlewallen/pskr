const popup = Handlebars.compile(`
  <strong>{{summary.callsign}}</strong><div>{{summary.packets.length}} packets</div>
  <ul>
  {{#each summary.vias}}
    <li>  {{#each this.path}} {{this.call}} {{/each}} ({{this.times}} times) {{ago this.last}}</li>
  {{/each}}
  </ul>
`);

const feedEntry = Handlebars.compile(`
  <li class="{{classes tags}}">
    <span class="callsign">{{packet.from}}</span>
  </li>
`);

const detailsHeader = Handlebars.compile(`<b>{{{packet.from}}}</b>`);

const detailsPosition = Handlebars.compile(`
  <div>
    Position: {{{coordinates packet.data.position.coordinates}}}
  </div>
`);

const detailsStatus = Handlebars.compile(`
  <div>
    {{#if packet.data.status.to}}
      <b>{{packet.data.status.to}}</b>:
    {{/if}}
    <span class="text">{{packet.data.status.text}}</span>
  </div>
`);

const detailsMessage = Handlebars.compile(`
  <div>
    {{#if packet.data.message.to}}
      <b>{{packet.data.message.to}}</b>:
    {{/if}}
    <span class="text">{{packet.data.message.text}}</span>
  </div>
`);

const detailsMicE = Handlebars.compile(`
  <div>
    MicE: {{{coordinates packet.data.micE.coordinates}}}
    <span class="message">{{packet.data.micE.message}}</span>
    <span class="text">{{packet.data.micE.text}}</span>
  </div>
`);

const detailsUnknown = Handlebars.compile(`
  <div>
    Unknown
  </div>
`);

function detailsBody(entry) {
  if (entry.packet.data.position) {
    return detailsPosition(entry);
  }
  if (entry.packet.data.message) {
    return detailsMessage(entry);
  }
  if (entry.packet.data.status) {
    return detailsStatus(entry);
  }
  if (entry.packet.data.micE) {
    return detailsMicE(entry);
  }
  return detailsUnknown(entry);
}

Handlebars.registerHelper("classes", function (value) {
  return value.join(" ");
});

Handlebars.registerHelper("ago", function (value) {
  return moment(value).fromNow();
});

Handlebars.registerHelper("coordinates", function (value) {
  const opts = { minimumFractionDigits: 5 };
  return `<span class="coordinates">${value[0].toLocaleString(
    "en-US",
    opts
  )}, ${value[1].toLocaleString("en-US", opts)}</span>`;
});

class Overview {
  constructor(map) {
    this.map = map;
    this.markers = {};
    this.positions = {};
    this.tracks = {};
  }

  emphasize(callsign) {
    $("#map .emphasize").removeClass("emphasize");
    const marker = this.markers[callsign];
    if (marker) {
      marker.addClassName("emphasize");
    } else {
      console.warn(`marker? ${callsign}`);
    }
  }

  deemphasize(callsign) {
    const marker = this.markers[callsign];
    if (marker) {
      marker.removeClassName("emphasize");
    } else {
      console.warn(`marker? ${callsign}`);
    }
  }

  createHopLines(summary) {
    const viaCalls = _(summary.vias)
      .map((v) => v.path)
      .flatten()
      .map((c) => c.call)
      .uniq()
      .value();

    const viaCoordinates = _(viaCalls)
      .map((call) => this.positions[call])
      .compact()
      .value();

    const center = this.positions[summary.callsign];

    // console.log("hover in", viaCalls, viaCoordinates);

    const lines = _(viaCoordinates)
      .map((end) => [center, end])
      .value();

    return lines;
  }

  createMarkerVisual(summary) {
    const size = 24;
    const i = document.createElement("i");
    i.classList = getSymbolClasses(summary, size);
    i.style.opacity = 1.0;

    return $("<div></div>")
      .append(
        $(i).hover(
          () => {
            // console.log("hover in", summary);

            this.map.addSource("route", {
              type: "geojson",
              data: {
                type: "Feature",
                properties: {},
                geometry: {
                  type: "MultiLineString",
                  coordinates: this.createHopLines(summary),
                },
              },
            });
            this.map.addLayer({
              id: "route",
              type: "line",
              source: "route",
              layout: {
                "line-join": "round",
                "line-cap": "round",
              },
              paint: {
                "line-color": "#888",
                "line-width": 2,
              },
            });
          },
          () => {
            this.map.removeLayer("route");
            this.map.removeSource("route");
          }
        )
      )
      .get(0);
  }

  getOrCreateMarker(summary) {
    const callsign = summary.callsign;
    if (!this.markers[callsign]) {
      const el = this.createMarkerVisual(summary);
      const marker = new maplibregl.Marker({ element: el });
      marker
        .setLngLat(summary.coordinates)
        .setPopup(new maplibregl.Popup().setHTML(popup({ summary: summary })));

      marker.addTo(this.map);
      this.markers[callsign] = marker;
    } else {
      this.markers[callsign]
        .setLngLat(summary.coordinates)
        .setPopup(new maplibregl.Popup().setHTML(popup({ summary: summary })));
    }
    return this.markers[callsign];
  }

  refresh() {
    fetch("data.json")
      .then((response) => response.json())
      .then((data) => {
        if (true) {
          showPskReporter(this.map, data);
        } else {
          Object.values(data.aprs.callsigns)
            .filter((summary) => summary.coordinates)
            .forEach((summary) => {
              // console.log(summary);
              this.initializeTrack(summary.callsign, summary.track);
              this.updatePosition(summary.callsign, summary.coordinates);
              this.getOrCreateMarker(summary);
            });
        }
      });
  }

  initializeTrack(callsign, track) {
    if (!this.tracks[callsign]) {
      this.tracks[callsign] = track;
      this.updateTrack(callsign);
    }
  }

  updatePosition(callsign, coordinates) {
    if (
      this.positions[callsign] &&
      !_.isEqual(this.positions[callsign], coordinates)
    ) {
      if (!this.tracks[callsign]) {
        this.tracks[callsign] = [this.positions[callsign]];
      }
      this.tracks[callsign].push(coordinates);
      console.log("tracks", this.tracks);
      this.updateTrack(callsign);
    }
    this.positions[callsign] = coordinates;
  }

  createTrackLines(callsign) {
    return [this.tracks[callsign]];
  }

  updateTrack(callsign) {
    if (this.map.getLayer(callsign)) {
      this.map.removeLayer(callsign);
      this.map.removeSource(callsign);
    }

    this.map.addSource(callsign, {
      type: "geojson",
      data: {
        type: "Feature",
        properties: {},
        geometry: {
          type: "MultiLineString",
          coordinates: this.createTrackLines(callsign),
        },
      },
    });
    this.map.addLayer({
      id: callsign,
      type: "line",
      source: callsign,
      layout: {
        "line-join": "round",
        "line-cap": "round",
      },
      paint: {
        "line-color": "#aa0000",
        "line-width": 2,
      },
    });
  }
}

function getPacketTags(event) {
  if (event.packet.data.position) {
    return ["position"];
  }
  if (event.packet.data.message) {
    return ["message"];
  }
  if (event.packet.data.status) {
    return ["status"];
  }
  if (event.packet.data.micE) {
    return ["mic-e"];
  }
  return ["unknown"];
}

class Feed {
  constructor(overview) {
    this.entries = [];
    this.overview = overview;
  }

  append(entry) {
    if (entry.event) {
      const decorated = _.extend(entry.event, {
        tags: getPacketTags(entry.event),
      });

      // console.log(decorated);

      this.entries.push(decorated);
    }
    return this;
  }

  render(container) {
    container.empty().append(
      _(_.clone(this.entries))
        .map((entry) =>
          $(feedEntry(entry))
            .hover(
              () => {
                // console.log("item hover:on", entry);
                this.overview.emphasize(entry.packet.from);
                $("#details .header").html(detailsHeader(entry));
                $("#details .body").html(detailsBody(entry));
                $("#details").show();
              },
              () => {
                this.overview.deemphasize(entry.packet.from);
                // console.log("item hover:off");
              }
            )
            .click(() => {
              // console.log("click");
              this.overview.map.setCenter(
                this.overview.positions[entry.packet.from]
              );
              /*
              this.overview.map.flyTo({
                center: this.overview.positions[entry.packet.from],
                zoom: 9,
                speed: 2.0,
                curve: 1,
                easing(t) {
                  return t;
                },
              });
              */
            })
        )
        .reverse()
        .value()
    );
  }
}

function initialize(style) {
  let protocol = new pmtiles.Protocol();
  maplibregl.addProtocol("pmtiles", protocol.tile);
  protocol.add(new pmtiles.PMTiles("map.pmtiles"));

  const zoom = 10;
  const lat = 33.941;
  const lon = -117.349;

  const map = new maplibregl.Map({
    container: "map",
    zoom: zoom,
    center: [lon, lat],
    style: style,
  });

  // map.showTileBoundaries = true;

  map.on("load", () => {
    const overview = new Overview(map);
    const feed = new Feed(overview);
    overview.refresh();

    const socket = new WebSocket("ws://127.0.0.1:3000/ws");
    socket.addEventListener("open", (event) => {});
    socket.addEventListener("message", (event) => {
      event.data.text().then((text) => {
        feed.append(JSON.parse(text)).render($("#feed ol"));
        overview.refresh();
      });
    });
  });
}

fetch("style.json")
  .then((response) => response.json())
  .then((style) => {
    initialize(style);
  });

function getSymbolClasses(summary, size) {
  if (summary.symbol == null) {
    return "dot";
  }
  const codes = summary.symbol.codes;
  const symbol = String.fromCharCode(codes[0]) + String.fromCharCode(codes[1]);
  const classes =
    getAPRSSymbolImageClasses(getAPRSSymbolAddress(symbol), size) + " size-24";
  return classes;
}

function showPskReporter(map, data) {
  if (false) {
    Object.values(data.pskr.locators).forEach((locator) => {
      const marker = new maplibregl.Marker({
        scale: 0.5,
        color: "#aaaaff",
      });
      marker
        .setLngLat(locator.coordinates)
        .setPopup(
          new maplibregl.Popup().setHTML(`<strong>${locator.locator}</strong>`)
        );
      marker.addTo(map);
    });
  }

  const squares = Object.values(data.pskr.locators).map((l) => l.square);

  map.addSource("squares", {
    type: "geojson",
    data: {
      type: "FeatureCollection",
      features: squares,
    },
  });
  map.addLayer({
    id: "squares",
    type: "fill",
    source: "squares",
    layout: {},
    paint: {
      "fill-color": ["get", "color"],
      "fill-opacity": 0.5,
    },
  });
}

use anyhow::Result;
use clap::Args;

#[derive(Debug, Args)]
pub struct Command {
    #[arg(short, long)]
    callsign: String,
    #[arg(short, long, default_value = "pskr.sqlite3")]
    db: String,
}

pub fn execute(cmd: Command) -> Result<()> {
    let pskr = crate::pskr::query_reports_of(&cmd.callsign, true)?;

    let db = crate::pskr::Database::new(cmd.db)?;

    db.create()?;

    crate::pskr::merge(&db, pskr)?;

    Ok(())
}

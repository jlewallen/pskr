use anyhow::Result;
use aprs_parser::{AprsPacket, DecodeError};
use chrono::{DateTime, Utc};
use itertools::Itertools;
use rusqlite::types::FromSql;
use serde::Serialize;
use std::collections::HashMap;
use tracing::*;

use super::db::{CallsignLatest, CallsignPosition, Packet, Received};

#[derive(Debug)]
pub struct DecodedAprsPacket {
    pub time: chrono::DateTime<Utc>,
    pub packet: AprsPacket,
}

#[derive(Debug)]
pub enum DecodeAttempt {
    Ok(DecodedAprsPacket),
    #[allow(dead_code)]
    Error(DecodeError),
}

impl TryFrom<Received> for DecodedAprsPacket {
    type Error = DecodeError;

    fn try_from(value: Received) -> Result<Self, Self::Error> {
        AprsPacket::decode_ax25(&value.data).map(|p| DecodedAprsPacket {
            time: value.time,
            packet: p,
        })
    }
}

impl From<Packet> for DecodeAttempt {
    fn from(value: Packet) -> Self {
        AprsPacket::decode_ax25(&value.data)
            .map(|p| {
                DecodeAttempt::Ok(DecodedAprsPacket {
                    time: value.received_at,
                    packet: p,
                })
            })
            .unwrap_or_else(DecodeAttempt::Error)
    }
}

#[derive(Debug, Serialize, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Callsign(String);

impl From<aprs_parser::Callsign> for Callsign {
    fn from(value: aprs_parser::Callsign) -> Self {
        Callsign(value.to_string())
    }
}

#[derive(Debug, Serialize, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct AprsVia {
    pub call: Callsign,
    pub heard: bool,
}

impl From<aprs_parser::Via> for AprsVia {
    fn from(value: aprs_parser::Via) -> Self {
        match value {
            aprs_parser::Via::Callsign(callsign, heard) => AprsVia {
                call: callsign.into(),
                heard,
            },
            aprs_parser::Via::QConstruct(_) => todo!(),
        }
    }
}

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Position {
    coordinates: (f64, f64),
    symbol: (u8, u8),
}

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Status {
    to: String,
    text: Option<String>,
}

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Message {
    to: String,
    text: Option<String>,
}

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct MicE {
    coordinates: (f64, f64),
    message: String,
    symbol: (u8, u8),
    text: String,
}

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub enum AprsData {
    Position(Position),
    Status(Status),
    Message(Message),
    MicE(MicE),
    Unknown,
}

impl From<aprs_parser::AprsData> for AprsData {
    fn from(value: aprs_parser::AprsData) -> Self {
        match value {
            aprs_parser::AprsData::Position(position) => AprsData::Position(Position {
                coordinates: (position.longitude.value(), position.latitude.value()),
                symbol: (position.symbol_table as u8, position.symbol_code as u8),
            }),
            aprs_parser::AprsData::Message(data) => {
                trace!("message: {:?}", data);

                let to = String::from_utf8(data.addressee.to_owned()).unwrap();
                let text = String::from_utf8(data.text.to_owned()).ok();

                Self::Message(Message { to, text })
            }
            aprs_parser::AprsData::Status(data) => {
                trace!("status: {:?}", data);

                let text = String::from_utf8(data.comment().to_owned()).ok();

                Self::Status(Status {
                    to: data.to.to_string(),
                    text,
                })
            }
            aprs_parser::AprsData::MicE(data) => {
                trace!("mic-e: {:?}", data);

                match String::from_utf8(data.comment.to_owned()) {
                    Ok(text) => Self::MicE(MicE {
                        coordinates: (data.longitude.value(), data.latitude.value()),
                        message: format!("{:?}", data.message),
                        symbol: (data.symbol_table, data.symbol_code),
                        text,
                    }),
                    Err(_) => Self::Unknown,
                }
            }
            aprs_parser::AprsData::Unknown(data) => {
                trace!("unknown: {:?}", data);

                Self::Unknown
            }
        }
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct AprsEvent {
    pub time: chrono::DateTime<Utc>,
    pub from: Callsign,
    pub via: Vec<AprsVia>,
    pub data: AprsData,
}

impl AprsEvent {
    fn coordinates(&self) -> Option<(f64, f64)> {
        match &self.data {
            AprsData::Position(position) => Some(position.coordinates),
            AprsData::Status(_) => None,
            AprsData::Message(_) => None,
            AprsData::MicE(me) => Some(me.coordinates),
            AprsData::Unknown => None,
        }
    }

    fn symbol(&self) -> Option<(u8, u8)> {
        match &self.data {
            AprsData::Position(position) => Some(position.symbol),
            AprsData::Status(_) => None,
            AprsData::Message(_) => None,
            AprsData::MicE(me) => Some(me.symbol),
            AprsData::Unknown => None,
        }
    }

    fn via(&self) -> Option<&Vec<AprsVia>> {
        if self.via.is_empty() {
            None
        } else {
            Some(&self.via)
        }
    }
}

impl From<DecodedAprsPacket> for AprsEvent {
    fn from(value: DecodedAprsPacket) -> Self {
        Self {
            time: value.time,
            from: value.packet.from.into(),
            via: value.packet.via.into_iter().map(|v| v.into()).collect(),
            data: value.packet.data.into(),
        }
    }
}

#[derive(Serialize)]
pub struct Data {
    callsigns: HashMap<String, CallSummary>,
}

pub fn load(db: &crate::aprs::Database) -> Result<Data> {
    let ax25_packets = db.query_recent_packets(chrono::Duration::hours(24))?;

    let positions: HashMap<_, Vec<CallsignPosition>> = db
        .query_positions(chrono::Duration::minutes(20))?
        .into_iter()
        .sorted_by_key(|r| r.callsign.clone()) // Dislike these clones.
        .group_by(|r| r.callsign.clone())
        .into_iter()
        .map(|(cs, tr)| (cs, tr.collect_vec()))
        .collect();

    let aprs_packets: Vec<DecodeAttempt> = ax25_packets
        .into_iter()
        .map(|p| p.into())
        .collect::<Vec<_>>()
        .into_iter()
        .collect();

    let by_callsign: HashMap<_, CallSummary> = aprs_packets
        .into_iter()
        .flat_map(|p| match p {
            DecodeAttempt::Ok(p) => Some(p),
            DecodeAttempt::Error(_) => None,
        })
        .sorted_by_key(|p| p.packet.from.to_string())
        .group_by(|p| p.packet.from.to_string())
        .into_iter()
        .map(|(call, packets)| {
            let packets: Vec<AprsEvent> = packets
                .sorted_by_key(|p| p.time)
                .rev()
                .map(|p| p.into())
                .collect_vec();

            let vias = packets
                .iter()
                .flat_map(|p| p.via().map(|v| (v, p.time)))
                .sorted_by_key(|r| r.0)
                .group_by(|r| r.0)
                .into_iter()
                .map(|(v, r)| {
                    let times = r.map(|r| r.1).sorted().rev().collect_vec();
                    ViaSummary {
                        path: v.clone(),
                        times: times.len(),
                        last: times.into_iter().max().expect("No times on grouped vias?"),
                    }
                })
                .sorted_by_key(|v| v.last)
                .rev()
                .collect::<Vec<_>>();

            let coordinates = packets.iter().flat_map(|p| p.coordinates()).next();
            let symbol = packets
                .iter()
                .flat_map(|p| p.symbol().map(Symbol::new))
                .next();
            let callsign = call.clone();

            let track = positions
                .get(&call)
                .map(|t| t.iter().map(|p| p.coordinates).collect_vec())
                .unwrap_or_default();

            (
                call,
                CallSummary {
                    callsign,
                    packets,
                    coordinates,
                    symbol,
                    vias,
                    track,
                },
            )
        })
        .collect();

    Ok(Data {
        callsigns: by_callsign,
    })
}

#[derive(Debug, Serialize)]
pub struct ViaSummary {
    pub path: Vec<AprsVia>,
    pub times: usize,
    pub last: DateTime<Utc>,
}

#[derive(Debug, Serialize)]
pub struct CallSummary {
    pub callsign: String,
    pub packets: Vec<AprsEvent>,
    pub coordinates: Option<(f64, f64)>,
    pub symbol: Option<Symbol>,
    pub vias: Vec<ViaSummary>,
    pub track: Vec<(f64, f64)>,
}

impl From<CallsignLatest> for CallSummary {
    fn from(value: CallsignLatest) -> Self {
        Self {
            callsign: value.callsign,
            packets: vec![],
            coordinates: value.longitude.zip(value.latitude),
            symbol: value.symbol,
            vias: vec![],
            track: vec![],
        }
    }
}

#[derive(Debug, Serialize)]
pub struct Symbol {
    pub codes: (u8, u8),
}

impl FromSql for Symbol {
    fn column_result(value: rusqlite::types::ValueRef<'_>) -> rusqlite::types::FromSqlResult<Self> {
        Vec::<u8>::column_result(value).map(Symbol::from)
    }
}

impl Symbol {
    pub fn new(codes: (u8, u8)) -> Symbol {
        Symbol { codes }
    }

    pub fn from_aprs_data(data: &aprs_parser::AprsData) -> Option<Symbol> {
        match data {
            aprs_parser::AprsData::Position(position) => Some(Symbol::new((
                position.symbol_table as u8,
                position.symbol_code as u8,
            ))),
            aprs_parser::AprsData::MicE(mic_e) => {
                Some(Symbol::new((mic_e.symbol_table, mic_e.symbol_code)))
            }
            _ => None,
        }
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        vec![self.codes.0, self.codes.1]
    }
}

impl From<Vec<u8>> for Symbol {
    fn from(value: Vec<u8>) -> Self {
        assert!(value.len() == 2);

        Self {
            codes: (value[0], value[1]),
        }
    }
}

use crate::aprs::Database;
use anyhow::Result;
use aprs_parser::{AprsData, AprsPacket};
use clap::Args;

use super::{db::Page, queries::Symbol};

#[derive(Debug, Args)]
pub struct Command {
    #[arg(short, long, default_value = "aprs.sqlite3")]
    db: String,
}

#[derive(Debug)]
pub struct Position(f64, f64);

impl Position {
    pub fn into_non_zero(self) -> Option<Self> {
        if self.0 == 0.0 && self.1 == 0.0 {
            None
        } else {
            Some(self)
        }
    }

    pub fn from_aprs_data(data: &AprsData) -> Option<Position> {
        match data {
            AprsData::Position(position) => {
                Self(position.longitude.value(), position.latitude.value()).into_non_zero()
            }
            AprsData::MicE(mic_e) => {
                Self(mic_e.longitude.value(), mic_e.latitude.value()).into_non_zero()
            }
            _ => None,
        }
    }

    pub fn longitude(&self) -> f64 {
        self.0
    }

    pub fn latitude(&self) -> f64 {
        self.1
    }
}

#[tokio::main]
pub async fn execute(cmd: Command) -> Result<()> {
    let db = Database::new(cmd.db)?;

    db.create()?;

    let mut page: Option<Page> = None;

    loop {
        db.begin()?;

        let (packets, following) = db.query_packets(page)?;

        for received in packets.iter() {
            if let Ok(packet) = AprsPacket::decode_ax25(&received.data) {
                let position = Position::from_aprs_data(&packet.data);
                let symbol = Symbol::from_aprs_data(&packet.data);

                db.add_or_update_callsign(
                    &packet.from.to_string(),
                    &received.received_at,
                    position,
                    symbol,
                )?;
            }
        }

        db.commit()?;

        if following.is_none() {
            break;
        }

        page = following;
    }

    Ok(())
}

use anyhow::{Context, Result};
use chrono::{DateTime, Duration, Utc};
use rusqlite::Connection;
use serde::Serialize;
use std::{ops::Sub, path::Path};
use tracing::*;

use super::{process::Position, queries::Symbol};

#[derive(Debug)]
pub struct Page(u64);

#[derive(Debug)]
pub struct Received {
    pub time: DateTime<Utc>,
    pub tnc: String,
    pub data: Vec<u8>,
}

#[derive(Debug)]
pub struct Packet {
    pub id: u64,
    pub received_at: chrono::DateTime<Utc>,
    #[allow(dead_code)]
    pub tnc: String,
    pub data: Vec<u8>,
}

#[derive(Debug, Serialize)]
pub struct CallsignLatest {
    pub id: u64,
    pub callsign: String,
    pub time: chrono::DateTime<Utc>,
    pub symbol: Option<Symbol>,
    pub longitude: Option<f64>,
    pub latitude: Option<f64>,
}

#[derive(Debug, Serialize)]
pub struct CallsignPosition {
    pub id: u64,
    pub callsign: String,
    pub time: chrono::DateTime<Utc>,
    pub coordinates: (f64, f64),
}

pub struct Database {
    conn: Connection,
}

impl Database {
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self> {
        Ok(Self {
            conn: Connection::open(path)?,
        })
    }

    pub fn create(&self) -> Result<()> {
        self.conn
            .execute(
                r#"
            CREATE TABLE IF NOT EXISTS packets (
                id INTEGER NOT NULL PRIMARY KEY,
                received_at TIMESTAMP NOT NULL,
                tnc TEXT NOT NULL,
                data BINARY NOT NULL
            )"#,
                [],
            )
            .with_context(|| "CREATE packets")?;

        self.conn
            .execute(
                r#"
            CREATE TABLE IF NOT EXISTS positions (
                id INTEGER NOT NULL PRIMARY KEY,
                callsign TEXT NOT NULL,
                time TIMESTAMP NOT NULL,
                longitude REAL NOT NULL,
                latitude REAL NOT NULL
            )"#,
                [],
            )
            .with_context(|| "CREATE positions")?;

        self.conn
            .execute(
                r#"
            CREATE UNIQUE INDEX IF NOT EXISTS positions_idx ON positions (callsign, time);
            "#,
                [],
            )
            .with_context(|| "CREATE positions_idx")?;

        self.conn
            .execute(
                r#"
            CREATE TABLE IF NOT EXISTS callsigns (
                id INTEGER NOT NULL PRIMARY KEY,
                callsign TEXT NOT NULL,
                time TIMESTAMP NOT NULL,
                symbol BINARY,
                longitude REAL,
                latitude REAL
            )"#,
                [],
            )
            .with_context(|| "CREATE callsigns")?;

        self.conn
            .execute(
                r#"
            CREATE UNIQUE INDEX IF NOT EXISTS callsigns_idx ON callsigns (callsign);
            "#,
                [],
            )
            .with_context(|| "CREATE callsigns_idx")?;

        Ok(())
    }

    pub fn begin(&self) -> Result<()> {
        self.conn.execute("BEGIN TRANSACTION", [])?;

        Ok(())
    }

    pub fn add_packet(&self, received: &Received) -> Result<()> {
        self.conn.execute(
            r#"
            INSERT INTO packets (
                received_at,
                tnc,
                data
            ) VALUES (?1, ?2, ?3)
            "#,
            (&received.time, &received.tnc, &received.data),
        )?;

        Ok(())
    }

    pub fn query_positions(&self, window: Duration) -> Result<Vec<CallsignPosition>> {
        let now = Utc::now();
        let after = now.sub(window);

        let mut stmt = self.conn.prepare(
            r"SELECT 
                id,
                callsign,
                time,
                longitude,
                latitude
            FROM positions
            WHERE time > ?1
            ORDER BY callsign, time DESC",
        )?;

        let res = stmt.query_map([&after], |row| {
            Ok(CallsignPosition {
                id: row.get(0)?,
                callsign: row.get(1)?,
                time: row.get(2)?,
                coordinates: (row.get(3)?, row.get(4)?),
            })
        })?;

        res.into_iter()
            .map(|row| Ok(row?))
            .collect::<Result<Vec<_>>>()
    }

    pub fn add_position(
        &self,
        callsign: &str,
        time: &DateTime<Utc>,
        coordinates: (f64, f64),
    ) -> Result<()> {
        self.conn.execute(
            r#"
            INSERT INTO positions (
                callsign,
                time,
                longitude,
                latitude
            ) VALUES (?1, ?2, ?3, ?4)
            ON CONFLICT (callsign, time) DO NOTHING
            "#,
            (callsign, time, coordinates.0, coordinates.1),
        )?;

        Ok(())
    }

    pub fn add_or_update_callsign(
        &self,
        callsign: &str,
        time: &DateTime<Utc>,
        coordinates: Option<Position>,
        symbol: Option<Symbol>,
    ) -> Result<()> {
        self.conn.execute(
            r#"
            INSERT INTO callsigns (callsign, time) VALUES (?1, ?2) ON CONFLICT (callsign) DO UPDATE SET time = excluded.time
            "#,
            (callsign, time),
        )?;

        if let Some(coordinates) = coordinates.as_ref() {
            self.add_position(
                callsign,
                time,
                (coordinates.longitude(), coordinates.latitude()),
            )?;

            self.conn.execute(
                r#"UPDATE callsigns SET longitude = ?2, latitude = ?3 WHERE callsign = ?1"#,
                (callsign, coordinates.longitude(), coordinates.latitude()),
            )?;
        }

        if let Some(symbol) = symbol.as_ref().map(|s| s.to_bytes()) {
            self.conn.execute(
                r#"UPDATE callsigns SET symbol = ?2 WHERE callsign = ?1"#,
                (callsign, symbol),
            )?;
        }

        Ok(())
    }

    pub fn query_packets(&self, page: Option<Page>) -> Result<(Vec<Packet>, Option<Page>)> {
        info!("querying {:?}", page);

        let mut stmt = self.conn.prepare(
            r"SELECT 
                id,
                received_at,
                tnc,
                data
            FROM packets
            WHERE id > ?1
            ORDER BY id
            LIMIT 1000",
        )?;

        let first_id = page.map(|p| p.0).unwrap_or_default();

        let res = stmt.query_map([&first_id], |row| {
            Ok(Packet {
                id: row.get(0)?,
                received_at: row.get(1)?,
                tnc: row.get(2)?,
                data: row.get(3)?,
            })
        })?;

        let packets = res
            .into_iter()
            .map(|row| Ok(row?))
            .collect::<Result<Vec<_>>>()?;

        let page = packets.iter().map(|p| p.id).max().map(Page);

        Ok((packets, page))
    }

    #[allow(dead_code)]
    pub fn query_callsigns(&self) -> Result<Vec<CallsignLatest>> {
        let mut stmt = self.conn.prepare(
            r"SELECT 
                id,
                callsign,
                time,
                symbol,
                longitude,
                latitude
            FROM callsigns
            ORDER BY time DESC",
        )?;

        let res = stmt.query_map([], |row| {
            Ok(CallsignLatest {
                id: row.get(0)?,
                callsign: row.get(1)?,
                time: row.get(2)?,
                symbol: row.get(3)?,
                longitude: row.get(4)?,
                latitude: row.get(5)?,
            })
        })?;

        res.into_iter()
            .map(|row| Ok(row?))
            .collect::<Result<Vec<_>>>()
    }

    pub fn query_recent_packets(&self, window: chrono::Duration) -> Result<Vec<Packet>> {
        let now = Utc::now();
        let after = now.sub(window);

        let mut stmt = self.conn.prepare(
            r"SELECT 
                id,
                received_at,
                tnc,
                data
            FROM packets
            WHERE received_at > ?1
            ORDER BY received_at DESC",
        )?;

        let res = stmt.query_map([&after], |row| {
            Ok(Packet {
                id: row.get(0)?,
                received_at: row.get(1)?,
                tnc: row.get(2)?,
                data: row.get(3)?,
            })
        })?;

        res.into_iter()
            .map(|row| Ok(row?))
            .collect::<Result<Vec<_>>>()
    }

    pub fn commit(&self) -> Result<()> {
        self.conn.execute("COMMIT TRANSACTION", [])?;

        Ok(())
    }
}

use crate::aprs::{process::Position, queries::Symbol, Database};
use anyhow::Result;
use aprs_parser::AprsPacket;
use chrono::Utc;
use clap::Args;
use kiss_tnc::Tnc;
use serde::Serialize;
use tokio::sync::broadcast::Sender;
use tracing::*;

use super::{
    db::Received,
    queries::{AprsEvent, DecodedAprsPacket},
};

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub enum TncEvent {
    Packet(AprsEvent),
}

#[derive(Debug, Args)]
pub struct Command {
    #[arg(short, long, default_value = "192.168.0.169:8001")]
    pub address: String,
    #[arg(short, long, default_value = "aprs.sqlite3")]
    pub db: Option<String>,
}

pub async fn watch(cmd: Command, publish: Option<&Sender<TncEvent>>) -> Result<()> {
    let mut tnc = Tnc::connect_tcp(&cmd.address).await?;

    let db: Option<Database> = match cmd.db {
        Some(path) => {
            let db = Database::new(path)?;
            db.create()?;
            Some(db)
        }
        None => None,
    };

    loop {
        let (_, data) = tnc.read_frame().await?;

        let received = Received {
            time: Utc::now(),
            tnc: cmd.address.to_owned(),
            data,
        };

        if let Some(db) = db.as_ref() {
            if let Err(e) = db.add_packet(&received) {
                error!("add_packet failed: {:?}", e)
            }

            match AprsPacket::decode_ax25(&received.data) {
                Ok(packet) => {
                    let position = Position::from_aprs_data(&packet.data);
                    let symbol = Symbol::from_aprs_data(&packet.data);

                    db.add_or_update_callsign(
                        &packet.from.to_string(),
                        &received.time,
                        position,
                        symbol,
                    )?;

                    info!("{:?}", packet);
                }
                Err(e) => {
                    warn!("{:?}", e);
                    warn!("{:?}", String::from_utf8_lossy(&received.data));
                }
            }
        }

        if let Some(publish) = publish.as_ref() {
            let decoded: Option<DecodedAprsPacket> = received.try_into().ok();
            let event: Option<AprsEvent> = decoded.map(|v| v.into());

            if let Some(event) = event {
                publish.send(TncEvent::Packet(event))?;
            }
        }
    }
}

#[tokio::main]
pub async fn execute(cmd: Command) -> Result<()> {
    watch(cmd, None).await
}

use anyhow::Result;
use clap::{Parser, Subcommand};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod aprs;
mod json;
mod load;
mod pskr;
mod query;
mod serve;

#[derive(Subcommand)]
pub enum Command {
    Query(query::Command),
    Load(load::Command),
    Json(json::Command),
    Serve(serve::Command),
    Tnc(aprs::tnc::Command),
    Process(aprs::process::Command),
}

#[derive(Parser)]
pub struct Cli {
    #[command(subcommand)]
    command: Command,
}

fn get_rust_log() -> String {
    let mut original = std::env::var("RUST_LOG").unwrap_or_else(|_| "info".into());

    if !original.contains("tower_http=") {
        original.push_str(",tower_http=info");
    }

    if !original.contains("hyper=") {
        original.push_str(",hyper=debug"); // temporary
    }

    original
}
fn main() -> Result<()> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(get_rust_log()))
        .with(tracing_subscriber::fmt::layer().with_thread_ids(false))
        .init();

    let cli = Cli::parse();

    match cli.command {
        Command::Query(cmd) => query::execute(cmd),
        Command::Load(cmd) => load::execute(cmd),
        Command::Json(cmd) => json::execute(cmd),
        Command::Serve(cmd) => serve::execute(cmd),
        Command::Tnc(cmd) => aprs::tnc::execute(cmd),
        Command::Process(cmd) => aprs::process::execute(cmd),
    }
}

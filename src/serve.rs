use anyhow::Result;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::response::Response;
use axum::routing::get_service;
use axum::Extension;
use axum::Json;
use axum_typed_websockets::Message;
use axum_typed_websockets::WebSocket;
use axum_typed_websockets::WebSocketUpgrade;
use clap::Args;
use futures_util::{SinkExt, StreamExt};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;
use tokio::signal;
use tokio::sync::broadcast::Sender;
use tokio::time::timeout;
use tower_http::services::ServeDir;
use tracing::*;

use tokio::fs::File;

use axum::{routing::get, Router};
use axum_extra::headers::Range;
use axum_extra::TypedHeader;
use tower_http::cors::{Any, CorsLayer};

use axum_range::KnownSize;
use axum_range::Ranged;
use tower_http::trace::DefaultMakeSpan;
use tower_http::trace::TraceLayer;

use crate::aprs::tnc;
use crate::aprs::tnc::TncEvent;
use crate::json;

#[derive(Debug, Args)]
pub struct Command {
    #[arg(long, default_value = "pskr.sqlite3")]
    pskr_db: PathBuf,
    #[arg(long, default_value = "aprs.sqlite3")]
    aprs_db: PathBuf,
    #[arg(long, default_value = "20231231.pmtiles")]
    tiles: PathBuf,
    #[arg(short, long)]
    address: Option<String>,
}

async fn file(
    Extension(cmd): Extension<Arc<Command>>,
    range: Option<TypedHeader<Range>>,
) -> Ranged<KnownSize<File>> {
    let file = File::open(&cmd.tiles).await.unwrap();
    let body = KnownSize::file(file).await.unwrap();
    let range = range.map(|TypedHeader(range)| range);
    Ranged::new(range, body)
}

async fn data(
    Extension(cmd): Extension<Arc<Command>>,
) -> Result<impl IntoResponse, (StatusCode, Json<serde_json::Value>)> {
    let json_cmd = json::Command {
        pskr_db: cmd.pskr_db.clone(),
        aprs_db: cmd.aprs_db.clone(),
    };

    let data = json::load(json_cmd).map_err(|e| {
        let error_response = serde_json::json!({
            "status": "fail",
            "message": format!("Error: {}", e),
        });
        (StatusCode::INTERNAL_SERVER_ERROR, Json(error_response))
    })?;

    let serialized = serde_json::to_string(&data).map_err(|e| {
        let error_response = serde_json::json!({
            "status": "fail",
            "message": format!("Error: {}", e),
        });
        (StatusCode::INTERNAL_SERVER_ERROR, Json(error_response))
    })?;

    Ok(Response::new(serialized))
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub enum ClientMessage {
    Token,
}

#[derive(Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
#[allow(dead_code)]
pub enum ServerMessage {
    Ping,
    Event(TncEvent),
}

#[derive(Debug)]
struct AppState {
    sender: Sender<TncEvent>,
}

async fn ws_handler(
    ws: WebSocketUpgrade<ServerMessage, ClientMessage>,
    Extension(state): Extension<Arc<AppState>>,
) -> impl IntoResponse {
    ws.on_upgrade(|socket| handle_socket(socket, state))
}

async fn handle_socket(stream: WebSocket<ServerMessage, ClientMessage>, state: Arc<AppState>) {
    let (mut sender, mut _from_ws) = stream.split();

    let mut from_tnc = state.sender.subscribe();

    loop {
        let maybe_message = timeout(Duration::from_millis(1000), from_tnc.recv()).await;

        match maybe_message {
            Ok(message) => match message {
                Ok(message) => {
                    if sender
                        .send(Message::Item(ServerMessage::Event(message)))
                        .await
                        .is_err()
                    {
                        warn!("sending:tx:error");
                        break;
                    }
                }
                Err(_) => break,
            },
            Err(e) => {
                debug!("ws-error: {:?}", e);
            }
        };
    }
}

#[tokio::main]
pub async fn execute(cmd: Command) -> Result<()> {
    let (sender, _receiver) = tokio::sync::broadcast::channel::<TncEvent>(10);

    if let Some(address) = cmd.address.clone() {
        tokio::spawn({
            let sender = sender.clone();
            async move {
                loop {
                    let r = tnc::watch(
                        tnc::Command {
                            address: address.clone(),
                            db: None,
                        },
                        Some(&sender),
                    )
                    .await;

                    warn!("tnc-task: {:?}", r);

                    tokio::time::sleep(Duration::from_secs(1)).await;
                }
            }
        });
    }

    let cors = CorsLayer::new()
        .allow_methods(Any)
        .allow_headers(Any)
        .allow_origin(Any);

    let app = Router::new()
        .fallback(get_service(
            ServeDir::new("public").append_index_html_on_directories(true),
        ))
        .route("/data.json", get(data))
        .route("/map.pmtiles", get(file))
        .route("/ws", get(ws_handler))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(DefaultMakeSpan::default().include_headers(false)),
        )
        .layer(Extension(Arc::new(cmd)))
        .layer(Extension(Arc::new(AppState { sender })))
        .layer(cors);

    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await?;
    info!("listening on {}", listener.local_addr()?);
    axum::serve(listener, app.into_make_service())
        .with_graceful_shutdown(shutdown_signal())
        .await?;

    Ok(())
}

async fn shutdown_signal() {
    let ctrl_c = async {
        signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }

    println!();

    info!("signal received, starting graceful shutdown");
}

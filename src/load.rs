use anyhow::Result;
use clap::Args;

#[derive(Debug, Args)]
pub struct Command {
    #[arg(short, long, default_value = "pskr.sqlite3")]
    pskr_db: String,
    #[arg(short, long, default_value = "aprs.sqlite3")]
    aprs_db: String,
}

pub fn execute(cmd: Command) -> Result<()> {
    let pskr = crate::pskr::parse(std::io::stdin())?;

    let db = crate::pskr::Database::new(cmd.pskr_db)?;

    db.create()?;

    crate::pskr::merge(&db, pskr)?;

    Ok(())
}

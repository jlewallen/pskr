use anyhow::Result;
use clap::Args;
use serde::Serialize;
use std::path::PathBuf;

use crate::aprs;
use crate::pskr;

#[derive(Debug, Args)]
pub struct Command {
    #[arg(short, long, default_value = "pskr.sqlite3")]
    pub pskr_db: PathBuf,
    #[arg(short, long, default_value = "aprs.sqlite3")]
    pub aprs_db: PathBuf,
}

#[derive(Serialize)]
pub struct Data {
    pub pskr: pskr::Data,
    pub aprs: aprs::Data,
}

pub fn load(cmd: Command) -> Result<Data> {
    let pskr_db = crate::pskr::Database::new(cmd.pskr_db)?;
    let aprs_db = crate::aprs::Database::new(cmd.aprs_db)?;

    Ok(Data {
        pskr: crate::pskr::load(&pskr_db)?,
        aprs: crate::aprs::load(&aprs_db)?,
    })
}

pub fn execute(cmd: Command) -> Result<()> {
    let data = load(cmd)?;

    println!("{}", serde_json::to_string_pretty(&data)?);

    Ok(())
}

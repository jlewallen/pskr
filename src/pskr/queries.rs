use anyhow::Result;
use geojson::{Feature, GeoJson, Geometry, Value};
use serde::Serialize;
use std::collections::HashMap;
use tracing::*;

use crate::pskr::Reception;

#[derive(Serialize)]
pub struct Data {
    receptions: Vec<Reception>,
    locators: HashMap<String, Locator>,
}

#[derive(Serialize)]
pub struct Locator {
    locator: String,
    properties: SquareProperties,
    coordinates: (f64, f64),
    square: GeoJson,
}

#[derive(Serialize)]
pub struct SquareProperties {
    average_snr: f64,
    average_snr_scaled: f64,
    receptions: usize,
    color: String,
}

#[derive(Debug)]
pub struct SnrScale {
    minimum: Option<i32>,
    maximum: Option<i32>,
}

impl SnrScale {
    fn new(receptions: &[Reception]) -> Self {
        let minimum = receptions.iter().flat_map(|r| r.snr).min();
        let maximum = receptions.iter().flat_map(|r| r.snr).max();
        Self { minimum, maximum }
    }

    fn scale(&self, value: &Option<f64>) -> Option<f64> {
        match (self.minimum, self.maximum, value) {
            (Some(min), Some(max), Some(value)) => Some((value - min as f64) / (max - min) as f64),
            (Some(_), Some(_), None) => None,
            _ => todo!(),
        }
    }
}

pub fn load(db: &crate::pskr::Database) -> Result<Data> {
    let receptions = db.query_receptions()?;

    use itertools::*;

    let scale = SnrScale::new(&receptions);

    info!("{:?}", scale);

    let scheme = colorous::INFERNO;

    let locators: HashMap<_, _> = receptions
        .iter()
        // Fun fact, `group_by` works on _consecutive_ runs.
        .sorted_by_key(|r| &r.receiver_locator)
        .group_by(|r| &r.receiver_locator[0..4])
        .into_iter()
        .map(|(mh, receptions)| {
            let receptions = receptions.collect_vec();
            let total = receptions.len();
            let average_snr =
                receptions.iter().flat_map(|r| r.snr).sum::<i32>() as f64 / total as f64;
            let scaled_snr = scale.scale(&Some(average_snr)).unwrap();
            (
                mh,
                SquareProperties {
                    average_snr,
                    average_snr_scaled: scaled_snr,
                    receptions: total,
                    color: format!("#{:x}", scheme.eval_continuous(scaled_snr)),
                },
            )
        })
        .collect();

    let locators = locators
        .into_iter()
        .map(|(mh, properties)| {
            let long_lat = maidenhead::grid_to_longlat(mh)?;
            let size = match mh.len() / 2 {
                4 => (1.0 / 8.0, 0.5 / 8.0),
                3 => (1.0 / 4.0, 0.5 / 4.0),
                2 => (1.0, 0.5),
                _ => (1.0, 0.5),
            };
            let nw = (long_lat.0 - size.0, long_lat.1 - size.1);
            let se = (long_lat.0 + size.0, long_lat.1 + size.1);

            let geometry = Geometry::new(Value::Polygon(vec![vec![
                vec![nw.0, nw.1],
                vec![se.0, nw.1],
                vec![se.0, se.1],
                vec![nw.0, se.1],
                vec![nw.0, nw.1],
            ]]));

            let square = GeoJson::Feature(Feature {
                bbox: None,
                geometry: Some(geometry),
                id: None,
                properties: Some(
                    serde_json::to_value(&properties)?
                        .as_object()
                        .unwrap()
                        .clone(),
                ),
                foreign_members: None,
            });

            Ok((
                mh.to_owned(),
                Locator {
                    locator: mh.to_owned(),
                    properties,
                    coordinates: long_lat,
                    square,
                },
            ))
        })
        .collect::<Result<HashMap<_, _>>>()?;

    Ok(Data {
        receptions,
        locators,
    })
}

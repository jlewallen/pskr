use std::io::{Read, Write};

use chrono::{DateTime, Utc};
use serde::Deserialize;
use thiserror::Error;

#[derive(Debug, Deserialize, Clone)]
#[allow(dead_code)]
pub struct ActiveReceiver {
    pub callsign: Option<String>,
    pub locator: String,
    pub frequency: Option<f64>,
    pub region: Option<String>,
    pub mode: String,
    #[serde(rename = "decoderSoftware")]
    pub decoder_software: String,
    #[serde(rename = "antennaInformation")]
    pub antenna_information: Option<String>,
    #[serde(rename = "DXCC")]
    pub dx_country: Option<String>,
}

#[derive(Debug, Clone)]
pub struct ActiveReceiverAtTime {
    pub time: DateTime<Utc>,
    pub receiver: ActiveReceiver,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(dead_code)]
pub struct ReceptionReport {
    #[serde(rename = "receiverCallsign")]
    pub receiver_callsign: Option<String>,
    #[serde(rename = "receiverLocator")]
    pub receiver_locator: String,
    #[serde(rename = "senderCallsign")]
    pub sender_callsign: String,
    #[serde(rename = "senderLocator")]
    pub sender_locator: Option<String>,
    #[serde(rename = "flowStartSeconds")]
    pub flow_start_seconds: i64,
    #[serde(rename = "isSender")]
    pub is_sender: u8,
    pub mode: String,
    pub frequency: Option<f64>,
    #[serde(rename = "sNR")]
    pub snr: Option<i32>,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(dead_code)]
pub struct ActiveCallsign {
    pub callsign: String,
    pub reports: u32,
    pub frequency: Option<f64>,
    #[serde(rename = "DXCC")]
    pub dx_country: Option<String>,
    #[serde(rename = "DXCCcode")]
    pub dx_code: String,
}

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
pub struct PskReport {
    #[serde(rename = "currentSeconds")]
    pub time: i64,
    #[serde(rename = "activeReceiver")]
    pub receivers: Vec<ActiveReceiver>,
    #[serde(rename = "receptionReport", default)]
    pub reports: Vec<ReceptionReport>,
    #[serde(rename = "activeCallsign")]
    pub active_callsigns: Vec<ActiveCallsign>,
}

impl PskReport {
    pub fn to_receivers_with_time(&self) -> Vec<ActiveReceiverAtTime> {
        let time = DateTime::from_timestamp(self.time, 0).unwrap();
        self.receivers
            .clone()
            .into_iter()
            .map(|receiver| ActiveReceiverAtTime { time, receiver })
            .collect()
    }
}

pub fn parse<R: Read>(reader: R) -> Result<PskReport, serde_xml_rs::Error> {
    serde_xml_rs::from_reader(reader)
}

pub fn query_reports_of(callsign: &str, always_write_file: bool) -> Result<PskReport, ApiError> {
    let url = format!(
        "https://retrieve.pskreporter.info/query?senderCallsign={}",
        callsign
    );

    let body = reqwest::blocking::get(url)?.text()?;

    let write_file = || {
        let filename = format!("reports_{}.xml", Utc::now().to_rfc3339());
        let mut file = std::fs::File::create(filename)?;
        file.write_all(body.as_bytes())
    };

    if always_write_file {
        write_file()?;
    }

    match serde_xml_rs::from_str(&body) {
        Ok(parsed) => Ok(parsed),
        Err(e) => {
            if !always_write_file {
                write_file()?;
            }

            Err(ApiError::Parse(e))
        }
    }
}

#[derive(Debug, Error)]
pub enum ApiError {
    #[error("io error")]
    Io(#[source] std::io::Error),
    #[error("http error")]
    Reqwest(#[source] reqwest::Error),
    #[error("parse error")]
    Parse(#[source] serde_xml_rs::Error),
}

impl From<std::io::Error> for ApiError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<reqwest::Error> for ApiError {
    fn from(value: reqwest::Error) -> Self {
        Self::Reqwest(value)
    }
}

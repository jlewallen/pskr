use anyhow::{Context, Result};
use chrono::{DateTime, Utc};
use rusqlite::Connection;
use serde::Serialize;
use std::path::Path;
use thiserror::Error;

pub struct Database {
    conn: Connection,
}

impl Database {
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self> {
        Ok(Self {
            conn: Connection::open(path)?,
        })
    }

    pub fn create(&self) -> Result<()> {
        self.conn
            .execute(
                r#"
            CREATE TABLE IF NOT EXISTS callsigns (
                callsign TEXT NOT NULL PRIMARY KEY,
                locator TEXT NOT NULL,
                region TEXT
            )"#,
                [],
            )
            .with_context(|| "CREATE callsigns")?;

        self.conn
            .execute(
                r#"
            CREATE UNIQUE INDEX IF NOT EXISTS callsigns_idx ON callsigns (callsign);
            "#,
                [],
            )
            .with_context(|| "CREATE callsigns_idx")?;

        self.conn
            .execute(
                r#"
            CREATE TABLE IF NOT EXISTS receivers (
                time TIMESTAMP NOT NULL,
                callsign TEXT NOT NULL,
                frequency FLOAT NOT NULL,
                mode TEXT NOT NULL
            );"#,
                [],
            )
            .with_context(|| "CREATE receivers")?;

        self.conn
                .execute(
                    r#"CREATE UNIQUE INDEX IF NOT EXISTS receivers_idx ON receivers (time, callsign, frequency, mode);"#,
                    [],
                )
                .with_context(|| "CREATE receivers_idx")?;

        self.conn
            .execute(
                r#"
            CREATE TABLE IF NOT EXISTS receptions (
                time TIMESTAMP NOT NULL,
                receiver_callsign TEXT NOT NULL,
                receiver_locator TEXT NOT NULL,
                sender_callsign TEXT NOT NULL,
                sender_locator TEXT NOT NULL,
                mode TEXT NOT NULL,
                frequency FLOAT,
                snr INTEGER
            );"#,
                [],
            )
            .with_context(|| "CREATE receptions")?;

        self.conn
                .execute(
                    r#"
            CREATE UNIQUE INDEX IF NOT EXISTS receptions_idx ON receptions (time, receiver_callsign, sender_callsign);
            "#,
                    [],
                )
                .with_context(|| "CREATE receptions_idx")?;

        Ok(())
    }

    pub fn merge_callsigns(&self, callsign: Vec<Callsign>) -> Result<()> {
        self.conn.execute("BEGIN TRANSACTION", [])?;

        for callsign in callsign.into_iter() {
            self.conn
                    .execute(
                        r#"
            INSERT INTO callsigns (
                callsign,
                locator,
                region
            ) VALUES (?1, ?2, ?3) ON CONFLICT (callsign) DO UPDATE SET locator = excluded.locator, region = excluded.region
            "#,
                        (&callsign.callsign, &callsign.locator, &callsign.region),
                    )?;
        }

        self.conn.execute("COMMIT TRANSACTION", [])?;

        Ok(())
    }

    pub fn merge_receivers(&self, receivers: Vec<Receiver>) -> Result<()> {
        self.conn.execute("BEGIN TRANSACTION", [])?;

        for receiver in receivers.into_iter() {
            self.conn.execute(
                r#"
            INSERT INTO receivers (
                time,
                callsign,
                frequency,
                mode
            ) VALUES (?1, ?2, ?3, ?4) ON CONFLICT (time, callsign, frequency, mode) DO NOTHING
            "#,
                (
                    &receiver.time,
                    &receiver.callsign,
                    &receiver.frequency,
                    &receiver.mode,
                ),
            )?;
        }

        self.conn.execute("COMMIT TRANSACTION", [])?;

        Ok(())
    }

    pub fn merge_receptions(&self, receptions: Vec<Reception>) -> Result<()> {
        self.conn.execute("BEGIN TRANSACTION", [])?;

        for reception in receptions.into_iter() {
            self.conn
                    .execute(
                        r#"
            INSERT INTO receptions (
                time,
                receiver_callsign,
                receiver_locator,
                sender_callsign,
                sender_locator,
                mode,
                frequency,
                snr
            ) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8) ON CONFLICT (time, receiver_callsign, sender_callsign) DO NOTHING
            "#,
                        (
                            &reception.time,
                            &reception.receiver_callsign,
                            &reception.receiver_locator,
                            &reception.sender_callsign,
                            &reception.sender_locator,
                            &reception.mode,
                            &reception.frequency,
                            &reception.snr,
                        ),
                    )
                    .with_context(|| "CREATE receptions_idx")?;
        }

        self.conn.execute("COMMIT TRANSACTION", [])?;

        Ok(())
    }

    #[allow(dead_code)]
    pub fn query_callsigns(&self) -> Result<Vec<Callsign>> {
        Ok(vec![])
    }

    #[allow(dead_code)]
    pub fn query_receivers(&self) -> Result<Vec<Receiver>> {
        Ok(vec![])
    }

    pub fn query_receptions(&self) -> Result<Vec<Reception>> {
        let mut stmt = self.conn.prepare(
            r"SELECT 
                time,
                receiver_callsign,
                receiver_locator,
                sender_callsign,
                sender_locator,
                mode,
                frequency,
                snr
            FROM receptions ORDER BY time DESC",
        )?;

        let res = stmt.query_map([], |row| {
            Ok(Reception {
                time: row.get(0)?,
                receiver_callsign: row.get(1)?,
                receiver_locator: row.get(2)?,
                sender_callsign: row.get(3)?,
                sender_locator: row.get(4)?,
                mode: row.get(5)?,
                frequency: row.get(6)?,
                snr: row.get(7)?,
            })
        })?;

        res.into_iter()
            .map(|row| Ok(row?))
            .collect::<Result<Vec<_>>>()
    }
}

#[derive(Serialize)]
pub struct Receiver {
    pub time: DateTime<Utc>,
    pub callsign: String,
    pub frequency: f64,
    pub mode: String,
}

impl TryFrom<crate::pskr::ActiveReceiverAtTime> for Receiver {
    type Error = ReportError;

    fn try_from(value: crate::pskr::ActiveReceiverAtTime) -> Result<Self, Self::Error> {
        Ok(Self {
            time: value.time,
            callsign: value
                .receiver
                .callsign
                .ok_or(ReportError::MissingCallsign)?,
            frequency: value
                .receiver
                .frequency
                .ok_or(ReportError::MissingFrequency)?,
            mode: value.receiver.mode,
        })
    }
}

#[derive(Debug, Error)]
pub enum ReportError {
    #[error("missing callsign")]
    MissingCallsign,
    #[error("missing frequency")]
    MissingFrequency,
}

#[derive(Serialize)]
pub struct Callsign {
    pub callsign: String,
    pub locator: String,
    pub region: Option<String>,
}

impl TryFrom<crate::pskr::ActiveReceiver> for Callsign {
    type Error = ReportError;

    fn try_from(value: crate::pskr::ActiveReceiver) -> std::prelude::v1::Result<Self, Self::Error> {
        Ok(Self {
            callsign: value.callsign.ok_or(ReportError::MissingCallsign)?,
            locator: value.locator,
            region: value.region,
        })
    }
}

#[derive(Serialize, Debug)]
pub struct Reception {
    pub time: chrono::DateTime<Utc>,
    pub receiver_callsign: String,
    pub sender_callsign: String,
    pub receiver_locator: String,
    pub sender_locator: String,
    pub mode: String,
    pub frequency: Option<f64>,
    pub snr: Option<i32>,
}

impl TryFrom<crate::pskr::ReceptionReport> for Reception {
    type Error = ReportError;

    fn try_from(
        value: crate::pskr::ReceptionReport,
    ) -> std::prelude::v1::Result<Self, Self::Error> {
        Ok(Self {
            time: DateTime::from_timestamp(value.flow_start_seconds, 0).unwrap(),
            receiver_callsign: value
                .receiver_callsign
                .ok_or(ReportError::MissingCallsign)?,
            sender_callsign: value.sender_callsign,
            receiver_locator: value.receiver_locator,
            sender_locator: value.sender_locator.unwrap_or_else(|| "".to_owned()), // TODO
            mode: value.mode,
            frequency: value.frequency,
            snr: value.snr,
        })
    }
}

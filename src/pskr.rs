mod api;
mod db;
mod queries;

use anyhow::Result;

pub use api::{
    parse, query_reports_of, ActiveReceiver, ActiveReceiverAtTime, PskReport, ReceptionReport,
};
pub use db::{Database, Reception};
pub use queries::{load, Data};

pub(crate) fn merge(db: &Database, pskr: PskReport) -> Result<()> {
    let callsigns = pskr
        .receivers
        .clone()
        .into_iter()
        .flat_map(|r| r.try_into())
        .collect();
    db.merge_callsigns(callsigns)?;

    let receivers = pskr
        .to_receivers_with_time()
        .into_iter()
        .flat_map(|r| r.try_into())
        .collect();
    db.merge_receivers(receivers)?;

    db.merge_receptions(
        pskr.reports
            .into_iter()
            .flat_map(|r| r.try_into())
            .collect(),
    )?;

    Ok(())
}

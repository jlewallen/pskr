# What is this?

Hello! This project started as a personal mirror for https://pskreporter.info/ and has evolved into the APRS space with functionality overlapping with APRSISCE32. Primarily this is just a learning exercise for myself, though I'm open to submissions.

- Web based GUI with rust backend.
- Offline maps support via [PMTiles](https://github.com/protomaps/PMTiles)
- SQLite backed storage.

# Features I'd Like

### Interactive Map Annotation

I would like for users to be able to add persisent custom markers/polygons to the map that other users can see.

### Additional TNC Transports

Right now the only transport supported is TCP/IP, developed against [Direwolf](https://github.com/wb2osz/direwolf).

### eof
